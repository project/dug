<?php
/**
 * @file
 * user_profile.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function user_profile_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: user-menu:node/add/blogmark
  $menu_links['user-menu:node/add/blogmark'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'node/add/blogmark',
    'router_path' => 'node/add/blogmark',
    'link_title' => 'Create a blogmark',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: user-menu:node/add/session
  $menu_links['user-menu:node/add/session'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'node/add/session',
    'router_path' => 'node/add/session',
    'link_title' => 'Create a session',
    'options' => array(
      'attributes' => array(
        'title' => 'Create a new session.',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: user-menu:node/add/suggest-a-session
  $menu_links['user-menu:node/add/suggest-a-session'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'node/add/suggest-a-session',
    'router_path' => 'node/add/suggest-a-session',
    'link_title' => 'Suggest a session',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: user-menu:user
  $menu_links['user-menu:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-10',
  );
  // Exported menu link: user-menu:user/logout
  $menu_links['user-menu:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '10',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Create a blogmark');
  t('Create a session');
  t('Log out');
  t('Suggest a session');
  t('User account');


  return $menu_links;
}
