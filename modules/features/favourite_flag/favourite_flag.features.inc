<?php
/**
 * @file
 * favourite_flag.features.inc
 */

/**
 * Implements hook_views_api().
 */
function favourite_flag_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function favourite_flag_flag_default_flags() {
  $flags = array();
  // Exported flag: "Favourite".
  $flags['favourite'] = array(
    'content_type' => 'node',
    'title' => 'Favourite',
    'global' => '0',
    'types' => array(
      0 => 'blogmark',
      1 => 'session',
    ),
    'flag_short' => 'Favourite',
    'flag_long' => 'Favourite this content.',
    'flag_message' => 'Content added to favourites',
    'unflag_short' => 'Unfavourite',
    'unflag_long' => 'Unfavourite this content.',
    'unflag_message' => 'Content has been removed from favourites.',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'roles' => array(
      'flag' => array(
        0 => '2',
      ),
      'unflag' => array(
        0 => '2',
      ),
    ),
    'weight' => 0,
    'show_on_form' => 1,
    'access_author' => '',
    'show_on_page' => 1,
    'show_on_teaser' => 1,
    'show_contextual_link' => 0,
    'i18n' => 0,
    'module' => 'favourite_flag',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 2,
  );
  return $flags;

}
