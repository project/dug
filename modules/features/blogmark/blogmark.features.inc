<?php
/**
 * @file
 * blogmark.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function blogmark_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function blogmark_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function blogmark_node_info() {
  $items = array(
    'blogmark' => array(
      'name' => t('Blogmark'),
      'base' => 'node_content',
      'description' => t('If you found something interesting and you want to share with other people.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
