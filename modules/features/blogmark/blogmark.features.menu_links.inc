<?php
/**
 * @file
 * blogmark.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function blogmark_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:blogmarks
  $menu_links['main-menu:blogmarks'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'blogmarks',
    'router_path' => 'blogmarks',
    'link_title' => 'Blogmarks',
    'options' => array(),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '3',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Blogmarks');


  return $menu_links;
}
