<?php
/**
 * @file
 * suggest_a_session.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function suggest_a_session_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function suggest_a_session_node_info() {
  $items = array(
    'suggest_a_session' => array(
      'name' => t('Suggest a session'),
      'base' => 'node_content',
      'description' => t('Suggest sessions to let the administrators know what you intrested in.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
