<?php
/**
 * @file
 * permissions_and_roles.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function permissions_and_roles_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
