; Drupal.org release file.
core = 7.x
projects[] = drupal
api = 2

; Contributed modules
projects[admin_menu][subdir] = contrib
projects[admin_menu][version] = 3.0-rc4

projects[auto_nodetitle][subdir] = contrib
projects[auto_nodetitle][version] = 1.0

projects[ctools][subdir] = contrib
projects[ctools][version] = 1.2

projects[date][subdir] = contrib
projects[date][version] = 2.6

projects[diff][subdir] = contrib
projects[diff][version] = 3.2

projects[ds][subdir] = contrib
projects[ds][version] = 2.2

projects[entity][subdir] = contrib
projects[entity][version] = 1.0

projects[entityreference][subdir] = contrib
projects[entityreference][version] = 1.0

projects[filefield_paths][subdir] = contrib
projects[filefield_paths][version] = 1.0-beta3

projects[features][subdir] = contrib
projects[features][version] = 2.0-beta1

projects[flag][subdir] = contrib
projects[flag][version] = 2.0

projects[google_analytics][subdir] = contrib
projects[google_analytics][version] = 1.3

projects[simple_gmap][subdir] = contrib
projects[simple_gmap][version] = 1.0

projects[l10n_update][subdir] = contrib
projects[l10n_update][version] = 2.0

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.1

projects[link][subdir] = contrib
projects[link][version] = 1.1

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 1.7

projects[panels][subdir] = contrib
projects[panels][version] = 3.3

projects[pathauto][subdir] = contrib
projects[pathauto][version] = 1.2

projects[rules][subdir] = contrib
projects[rules][version] = 2.2

projects[social_profile_field][subdir] = contrib
projects[social_profile_field][version] = 7.x-1.0-beta1

projects[strongarm][subdir] = contrib
projects[strongarm][version] = 2.0

projects[token][subdir] = contrib
projects[token][version] = 1.5

projects[views][subdir] = contrib
projects[views][version] = 3.5

projects[wysiwyg][subdir] = contrib
projects[wysiwyg][version] = 2.2


projects[twitter_pull][subdir] = contrib
projects[twitter_pull[version] = 7.x-1.0-rc4

; Libraries
libraries[ckeditor][download][type] = get
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%203.6.2/ckeditor_3.6.2.tar.gz"
libraries[ckeditor][destination] = libraries
libraries[ckeditor][directory_name] = ckeditor
