<?php
/**
 * @file
 * Implements the distribution's custom functions.
 */

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dug_form_install_configure_form_alter(&$form, &$form_state)  {
  // Add checkbox so that the admin can decide that he want to set the GA on the site in site install.
  $form['googleanalytics'] = array(
    '#type' => 'fieldset',
    '#title' => st('Google Analytics'),
  );
  $form['googleanalytics']['googleanalytics'] = array(
    '#type' => 'checkbox',
    '#title' => st('Configure Google analytics'),
    '#description' => st('Check if you wanted to configure the google analytics on the site.'),
  );
  $form['#submit'][] = 'dug_configure_ga_submit';

  // Add config to set tweet hash tags which belong to tweets displaying on the website.
  $form['tweets'] = array(
    '#type' => 'fieldset',
    '#title' => st('Tweets'),
  );
  $form['tweets']['tweet_hash'] = array(
    '#type' => 'textfield',
    '#title' => st('Hash tag'),
    '#description' => st('Add hash tag which belong to tweets displaying on the site.'),
  );
  $form['#submit'][] = 'dug_configure_tweet_hash_submit';
}

/**
 * Submit handler to store if the admin wants to configure GA in site install.
 *
 * @see dug_form_install_configure_form_alter()
 */
function dug_configure_ga_submit(&$form, &$form_state) {
  if ($form_state['values']['googleanalytics']) {
    variable_set('dug_configure_googleanalytics', $form_state['values']['googleanalytics']);
  }
}

/**
 * Submit handler to hash tags which belong to tweets displaying on the website.
 *
 * @see dug_form_install_configure_form_alter()
 */
function dug_configure_tweet_hash_submit(&$form, &$form_state) {
  if ($form_state['values']['tweet_hash']) {
    variable_set('dug_configure_tweet_hash', $form_state['values']['tweet_hash']);
  }
}

/**
 * Implements hook_twitter_pull_blocks().
 */
function dug_twitter_pull_blocks() {
  return array(
    0 => (object) array(
      'delta' => 'dug_tweets_0',
      'tweetkey' => variable_get('dug_configure_tweet_hash', 'dug'),
      'title' => t('Fresh'),
      'name' => 'Fresh DUG tweets',
      'number_of_items' => variable_get('dug_tweets_num', 3),
    ),
  );
}
